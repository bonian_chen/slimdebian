FROM debian:bullseye-slim

RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get install -y curl lz4 brotli ninja-build expect-dev inotify-tools gnupg gnupg2
RUN curl https://packages.fluentbit.io/fluentbit.key | gpg --dearmor > /usr/share/keyrings/fluentbit-keyring.gpg
RUN echo 'deb [signed-by=/usr/share/keyrings/fluentbit-keyring.gpg] https://packages.fluentbit.io/debian/bullseye bullseye main' >> /etc/apt/sources.list
RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get install -y fluent-bit && apt-get clean && rm -rf /var/lib/apt/lists/*;

